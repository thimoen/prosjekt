package app;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class AppController implements Initializable {
	@FXML
	private TableView<Book> mainTable;
	@FXML
	private TableColumn<Book, String> titleC;
	@FXML
	private TableColumn<Book, Integer> yearC;
	@FXML
	private TableColumn<Book, String> authorC;
	@FXML
	private TableColumn<Book, Long> isbnC;
	
	final ObservableList<Book> data =
			FXCollections.observableArrayList(
					new Book("pepe", 13, "hands", 100000000000000000L)
			);
  
	public static ObservableList<Book> getBook(){
		ObservableList<Book> allBooks = FXCollections.observableArrayList();
		List<Book> temp = App.getLibrary();
		for (Book bok : temp) {
			allBooks.add(bok);
		}
		return allBooks;
	}
	
	@FXML
	void switchScreen1(ActionEvent event) throws IOException {
		Stage window  = (Stage)(((Node) event.getSource()).getScene().getWindow());
		Parent addScreenParent = FXMLLoader.load(App.class.getResource("AddScreen.fxml"));
		Scene addScreenScene = new Scene(addScreenParent);
		window.setScene(addScreenScene);
		window.show();
	}
	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		titleC.setCellValueFactory(new PropertyValueFactory<>("title"));
		yearC.setCellValueFactory(new PropertyValueFactory<>("year"));
		authorC.setCellValueFactory(new PropertyValueFactory<>("author"));
		isbnC.setCellValueFactory(new PropertyValueFactory<>("ISBN"));
		mainTable.getItems().setAll(getBook());
	}
	
}
