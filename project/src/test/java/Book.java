package app;

public class Book {
	String title;
	int year;
	String author;
	long ISBN;

	public Book(String title, int year, String author, long iSBN) {
		super();
		this.title = title;
		this.year = year;
		this.author = author;
		ISBN = iSBN;
	}
	
	public String getTitle() {
		return title;
	}

	public int getYear() {
		return year;
	}

	public String getAuthor() {
		return author;
	}

	public long getISBN() {
		return ISBN;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", year=" + year + ", author=" + author + ", ISBN=" + ISBN + "]";
	}
	
	
}
