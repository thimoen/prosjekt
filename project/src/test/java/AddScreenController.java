package app;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AddScreenController {
	
	@FXML
	TextField btitle;
	@FXML
	TextField byear;
	@FXML
	TextField bauthor;
	@FXML
	TextField bisbn;
	@FXML
	Label berror;
		
	
	@FXML
	void switchScreen2(ActionEvent event) throws IOException {
		Stage window  = (Stage)(((Node) event.getSource()).getScene().getWindow());
		Parent addScreenParent = FXMLLoader.load(App.class.getResource("App.fxml"));
		Scene addScreenScene = new Scene(addScreenParent);
		window.setScene(addScreenScene);
		window.show();
	}
	@FXML
	void addBook() {
		Boolean validTi = notEmpty(btitle.getText());
		Boolean validYr = validateInt(byear.getText());
		Boolean validAu = validateString(bauthor.getText());
		Boolean validIS = validateISBN(bisbn.getText());
		
		if (validTi && validYr && validAu && validIS) {
			String title = btitle.getText();
			int year = toInt(byear.getText());
			String author = bauthor.getText();
			long ISBN = toLong(bisbn.getText());
			validBook();
			Book newBook = new Book(title, year, author, ISBN);
			App.bookToLibrary(newBook);
			} else {
			notValidBook();
		}
	}
	
	@FXML
	void notValidBook() {
		berror.setTextFill(Color.web("#a30000"));
		berror.setText("Not valid inputs");
	}
	void validBook() {
		berror.setText("Book added");
		berror.setTextFill(Color.web("#3be62c"));
		btitle.clear();
		byear.clear();
		bauthor.clear();
		bisbn.clear();
	}
	
    public Boolean validateString(String test) {
        return test.matches("^[������A-Za-z ]*$") && !test.matches("^\\s*$");
    }
	
	private Boolean validateInt(String test) {
		return test.matches("^[0-9]*$") && test.length() < 11;
	}
	
	private Boolean validateISBN(String isbn) {
        return isbn.matches("^[0-9]*$") && isbn.length() == 13;
    }
	public Boolean notEmpty(String test) {
        return !test.matches("^\\s*$");
    }
	
	
	// Number conversion
	public int toInt(String c) {
		return Integer.parseInt(c);
	}
	public long toLong(String c) {
		return Long.parseLong(c);
	}
	
}
