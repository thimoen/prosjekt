package app;

import java.util.ArrayList;
import java.util.List;


public class Library {
	private List<Book> books = new ArrayList<Book>();
	
	public void addBook(Book book1) {
		books.add(book1);
	}

	public List<Book> getBooks() {
		return books;
	}
	
}
